/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const int startwithgaps[]    = { 1 };	/* 1 means gaps are used by default, this can be customized for each tag */
static const unsigned int gappx[]   = { 10 };   /* default gap between windows in pixels, this can be customized for each tag */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Unifont:size=12" };
static const char dmenufont[]       = "Unifont:size=12";
static const char col_1[]  = "#131312"; /* background color of bar */
static const char col_2[]  = "#B51919"; /* border color unfocused windows */
static const char col_3[]  = "#d7d7d7";
static const char col_4[]  = "#81503D"; /* border color focused windows and tags */
static const unsigned int baralpha    = 0xff; 
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]        = {
	[SchemeNorm] = { col_3, col_1, col_1 },
	[SchemeSel]  = { col_1, col_3, col_3 },
};
static const unsigned int alphas[][3] = {
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};
/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      	     instance    title    tags mask     isfloating   CenterThisWindow?     monitor */
	{ "st",              NULL,       NULL,    0,            0,     	     1,		           -1 },
	{ "Gimp",            NULL,       NULL,    0,            1,           0,                    -1 },
	{ "Firefox",         NULL,       NULL,    1 << 8,       0,           0,                    -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(CHAIN,KEY,TAG) \
	{ MODKEY,                       CHAIN,    KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           CHAIN,    KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             CHAIN,    KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, CHAIN,    KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]    = { "dmenu_run", "-p", "Run: ", "-nf", "Unifont" };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     chain key   key        function        argument */
	{ MODKEY,                       -1,         XK_d,      spawn,          SHCMD("dmenu_run -fn Unifont -p Run: ") },
	{ MODKEY,                       -1,         XK_t,      spawn,          {.v = termcmd } },
	{ MODKEY,                       -1,         XK_b,      togglebar,      {0} },
	{ MODKEY,                       -1,         XK_o,      focusstack,     {.i = +1 } },
	{ MODKEY,                       -1,         XK_n,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             -1,         XK_o,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             -1,         XK_n,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       -1,         XK_j,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       -1,         XK_k,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       -1,         XK_e,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       -1,         XK_i,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       -1,         XK_Return, zoom,           {0} },
	{ MODKEY,                       -1,         XK_Tab,    view,           {0} },
	{ MODKEY,                       -1,         XK_c,      killclient,     {0} },
	{ MODKEY,                       -1,         XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             -1,         XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       -1,         XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       -1,         XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             -1,         XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             -1,         XK_period, tagmon,         {.i = +1 } },

	{ MODKEY,                        XK_l,      XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                        XK_l,      XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                        XK_l,      XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                        XK_l,      XK_space,  setlayout,      {0} },

	{ MODKEY,                        XK_u,      XK_n,     spawn,           SHCMD("cmus-remote --previous") },
	{ MODKEY,                        XK_u,      XK_o,     spawn,           SHCMD("cmus-remote --next") },
	{ MODKEY,                        XK_u,      XK_p,     spawn,           SHCMD("cmus-remote --pause") },
	{ MODKEY,                        XK_u,      XK_space, spawn,           SHCMD("cmus-remote --play") },

	{ MODKEY,                        XK_p,      XK_p,      spawn,          SHCMD("bitwarden-dmenu") },
	{ MODKEY,                        XK_p,      XK_x,      spawn,          SHCMD("maim -s | xclip -selection clipboard -t image/png") },
	{ MODKEY,                        XK_p,      XK_k,      spawn,          SHCMD("dm-kill") },
	{ MODKEY,                        XK_p,      XK_s,      spawn,          SHCMD("dm-search") },
	{ MODKEY,                        XK_p,      XK_e,      spawn,          SHCMD("dm-confedit") },
	{ MODKEY,                        XK_p,      XK_q,      spawn,          SHCMD("dm-logout") },
	{ MODKEY,                        XK_p,      XK_m,      spawn,          SHCMD("dm-man") },
	{ MODKEY,                        XK_p,      XK_t,      spawn,          SHCMD("dm-translate") },
	{ MODKEY,                        XK_p,      XK_b,      spawn,          SHCMD("dm-bookman") },

	TAGKEYS(                        -1,         XK_1,                      0)
	TAGKEYS(                        -1,         XK_2,                      1)
	TAGKEYS(                        -1,         XK_3,                      2)
	TAGKEYS(                        -1,         XK_4,                      3)
	TAGKEYS(                        -1,         XK_5,                      4)
	TAGKEYS(                        -1,         XK_6,                      5)
	TAGKEYS(                        -1,         XK_7,                      6)
	TAGKEYS(                        -1,         XK_8,                      7)
	TAGKEYS(                        -1,         XK_9,                      8)
	{ MODKEY|ShiftMask,             -1,         XK_q,      quit,           {0} },
	{ MODKEY,                       XK_a,       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_a,       XK_t,      spawn,          {.v = termcmd } },
};
/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

